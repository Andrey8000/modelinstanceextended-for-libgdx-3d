package your package;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;

/**
 * ���������� � api ModelInstance � libgdx 3d - ������ �����������, �������� � ��������������� � ������� ����������
 * <br>
 * <br>
 * Copyright (c) 2014 Andrey Riyk (Crocodile8000)
 * <br>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <br>
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * <br>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * <br>
 * Except as contained in this notice, 
 * the name(s) of the above copyright holders shall not be used in advertising 
 * or otherwise to promote the sale, use or other dealings 
 * in this Software without prior written authorization.
 */
public class ModelInstanceExtended {
	
	private ModelInstance modelInstance;
	private float xScale, yScale, zScale;
	private float xPosition, yPosition, zPosition;
	private float xRotation, yRotation, zRotation;

	public ModelInstanceExtended(Model model){
		modelInstance = new ModelInstance(model);
		xScale = yScale = zScale = 1;
	}
	
	public ModelInstance getModelInstance(){
		return modelInstance;
	}
	
	public float getxPosition() {
		return xPosition;
	}

	public float getyPosition() {
		return yPosition;
	}

	public float getzPosition() {
		return zPosition;
	}

	/**
	 * ����������� ������, Diff - �������� ������������ ������� �������
	 * <br>����� �� ���������������
	 */
	public void moveDiff(float x, float y, float z){
		// ������������ ������ � �������� ���������
		modelInstance.transform.rotate(Vector3.Z, -zRotation);
		modelInstance.transform.rotate(Vector3.Y, -yRotation);
		modelInstance.transform.rotate(Vector3.X, -xRotation);
		// ��������� ������� ��� ��������� ��� ��������
		x *= (1f/xScale);
		y *= (1f/yScale);
		z *= (1f/zScale);
		// ������� ������
		modelInstance.transform.translate(x, y, z);
		// ��������� ��������
		xPosition += x;
		yPosition += y;
		zPosition += z;
		// ������������ ������ �������
		modelInstance.transform.rotate(Vector3.X, xRotation);
		modelInstance.transform.rotate(Vector3.Y, yRotation);
		modelInstance.transform.rotate(Vector3.Z, zRotation);
	}

	/**
	 * ������ ������� ������� � ���������� ��������� (�� ������������ ��������)
	 * <br>����� �� ���������������
	 */
	public void rotateAbsolute(float x, float y, float z){
		// ������������ ������ � �������� ���������
		modelInstance.transform.rotate(Vector3.Z, -zRotation);
		modelInstance.transform.rotate(Vector3.Y, -yRotation);
		modelInstance.transform.rotate(Vector3.X, -xRotation);
		// ��������� ��������
		xRotation = x;
		yRotation = y;
		zRotation = z;
		// ������������ �� ����� ��������
		modelInstance.transform.rotate(Vector3.X, xRotation);
		modelInstance.transform.rotate(Vector3.Y, yRotation);
		modelInstance.transform.rotate(Vector3.Z, zRotation);
	}

	/**
	 * ������ ������� ������� � ���������� ��������� (�� ������������ ��������)
	 * <br>����� �� ���������������
	 */
	public void scaleAbsolute(float x, float y, float z){
		// ������������ ������ � �������� ���������
		modelInstance.transform.rotate(Vector3.Z, -zRotation);
		modelInstance.transform.rotate(Vector3.Y, -yRotation);
		modelInstance.transform.rotate(Vector3.X, -xRotation);
		// ���������� ������� �������
		modelInstance.transform.scale(1f/xScale ,1f/yScale ,1f/zScale);
		// ������ ����� �������
		modelInstance.transform.scale(x ,y ,z);
		xScale = x;
		yScale = y;
		zScale = z;
		// ������������ ������ �������
		modelInstance.transform.rotate(Vector3.X, xRotation);
		modelInstance.transform.rotate(Vector3.Y, yRotation);
		modelInstance.transform.rotate(Vector3.Z, zRotation);
	}
	
}
